<?php

namespace App\Controllers;

use SteeveDroz\CiBase\BaseController as Controller;

class BaseController extends Controller
{
    public function init()
    {
        // Execute here the code that needs to be loaded on each page
    }
}
