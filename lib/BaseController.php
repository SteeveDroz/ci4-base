<?php

/**
 * This file is created by Steeve Droz and is part of the Ci4Base package.
 */

namespace SteeveDroz\CiBase;

use CodeIgniter\Controller;
use Config\App;

/**
 * Class that allows using templates and the parser with more ease than in vanilla CodeIgniter 4.
 *
 * App\Controllers\BaseController should extend this class. The constructor is replaced by {@link BaseController::init}.
 */
abstract class BaseController extends Controller
{
    /**
     * Unused
     *
     * @var array
     */
    protected $helpers = [];

    /**
     * Contains all the data to display in the view.
     *
     * @var array
     */
    protected $data = [];

    /**
     * Contains the data that already have been escaped, use with caution.
     *
     * @var array
     */
    protected $rawData = [];

    /**
     * Contains the data about CSS.
     *
     * @var array
     */
    protected $css = [];

    /**
     * Contains the data about JS.
     *
     * @var array
     */
    protected $js = [];

    /**
     * Contains the literal lines of JS code that should be added to the HEAD.
     *
     * @var array
     */
    protected $javascript = [];

    /**
     * The default template
     *
     * @var string
     */
    protected $template = 'template';

    /**
     * Size of backtrace between the call of $this->render() and this file.
     *
     * @var int
     */
    protected $backtraceLength = 1;

    /**
     * The redefinition of this inherited field is only useful for Intelephense.
     *
     * Intelephense has trouble recognizing the correct type for this field.
     *
     * @var \CodeIgniter\HTTP\IncomingRequest
     */
    protected $request;

    /**
     * Default method found in the original BaseController.
     *
     * The only addition is the call of `$this->init()` to use it as a constructor.
     *
     * @param \CodeIgniter\HTTP\RequestInterface  $request  The request
     * @param \CodeIgniter\HTTP\ResponseInterface $response The response
     * @param \Psr\Log\LoggerInterface            $logger   The logger
     *
     * @return void
     *
     * @codeCoverageIgnore
     */
    public function initController(\CodeIgniter\HTTP\RequestInterface $request, \CodeIgniter\HTTP\ResponseInterface $response, \Psr\Log\LoggerInterface $logger)
    {
        parent::initController($request, $response, $logger);
        $this->init();
    }

    /**
     * Changes the template to use with the controllers.
     *
     * @param string $template The location of the HTML file relative to VIEWPATH. The ".html" can optionally be omitted.
     */
    protected function setTemplate(string $template): void
    {
        if ('.html' === substr($template, -5)) {
            $this->template = substr($template, 0, -5);
        } else {
            $this->template = $template;
        }
    }

    /**
     * Parses a file and returns the content.
     *
     * This method parses the content of a page and injects the data located in $this->data (escaped) and in $this->rawData (unescaped).
     *
     * @param string $page The page to display, relative to VIEWPATH. The ".html" can optionally be omitted.
     *
     * @return string the content injected with data
     */
    protected function fragment(string $page): string
    {
        $parser = service('parser');

        if ('.html' !== substr($page, -5)) {
            $page .= '.html';
        }
        $data = $this->data;
        uksort($this->data, static function ($a, $b) use ($data) {
            if (is_array($data[$a]) && ! is_array($data[$b])) {
                return -1;
            }
            if (is_array($data[$b]) && ! is_array($data[$a])) {
                return 1;
            }

            return 0;
        });

        try {
            $output = $parser
                ->setData($this->data)
                ->setData($this->rawData, 'raw')
                ->render($page);

            return preg_replace('/\r?\n$/', '', $output);
        } catch (\CodeIgniter\View\Exceptions\ViewException $e) {
            throw new \CodeIgniter\View\Exceptions\ViewException('Missing file: ' . APPPATH . 'Views/' . $page);
        }
    }

    /**
     * Loads the template file with the corresponding page to display.
     *
     * This method first fetches the content of the inner page and injects the data located in $this->data (escaped) and in $this->rawData (unescaped). It then takes that result and injects it in the template, in place of the {template_content} tag.
     *
     * If the $page parameter is null, it is calculated from the controller and method that called $this->render() and finds the file in the corresponding location: VIEWPATH/[controller]/[method].html.
     * For example, if called from User::login(), it will load the file VIEWPATH/user/login.html.
     *
     * If the parameter is a string, it uses this file, relative to VIEWPATH. The ".html" can optionally be omitted.
     *
     * Once the page is ready, it is sent to the client as a response.
     *
     * @param string $page the page to display, by default the one corresponding to the calling controller (see above)
     */
    protected function render(?string $page = null): void
    {
        $parser = service('parser');
        $app    = new App();

        $this->rawData['template_css'] = implode(PHP_EOL . '    ', array_map(static fn ($style) => '<link rel="stylesheet" href="' . $style['uri'] . '" type="text/css" media="' . $style['media'] . '">', $this->css));

        $this->rawData['template_js'] = implode(PHP_EOL . '    ', array_map(static function ($script) {
            $additionalParameters = '';

            foreach ($script['additionalParameters'] as $attribute => $value) {
                $additionalParameters .= ' ' . $attribute . '="' . $value . '"';
            }

            return '<script src="' . $script['uri'] . '"' . ($script['defer'] ? ' defer' : '') . $additionalParameters . '></script>';
        }, $this->js));

        $this->rawData['template_javascript'] = '<script>' . PHP_EOL . '        ' . implode(PHP_EOL . '        ', $this->javascript) . PHP_EOL . PHP_EOL . '    </script>';

        $siteName                          = $app->siteName ?? '\Config\App::siteName';
        $this->data['template_page_title'] = array_key_exists('page_title', $this->data) ? $this->data['page_title'] . ' - ' . $siteName : $siteName;

        if (null === $page) {
            $caller = debug_backtrace(0, $this->backtraceLength + 1)[$this->backtraceLength];

            $classParts = explode('\\', $caller['class']);
            $function   = $caller['function'];

            if (substr($function, 0, 3) === 'get') {
                $function = strtolower(substr($function, 3, 1)) . substr($function, 4);
            } elseif (substr($function, 0, 4) === 'post') {
                $function = strtolower(substr($function, 4, 1)) . substr($function, 5);
            }

            $page = strtolower(end($classParts)) . '/' . $function;
        }

        $this->rawData['template_content'] = $this->fragment($page);
        $output                            = $this->fragment($this->template) . PHP_EOL;

        echo $output;
    }

    /**
     * Adds a new CSS file to the current list, in order to display it when calling $this->render().
     *
     * The name of the file is either an absolute path (when starting with https:// or other protocols) or otherwise a path relative to FCPATH/css/.
     *
     * All the CSS will be included in the template, in place of the {template_css} tag.
     *
     * @param string $uri   The relative or absolute path. The ".css" can optionally be omitted.
     * @param string $media the media on which to apply the css: all (default), screen, print, speech
     */
    protected function css(string $uri, string $media = 'all'): void
    {
        if (! preg_match('#^(\w+:)?//#', $uri)) {
            $app = new App();
            $uri = $app->baseURL . ('/' === substr($app->baseURL, -1) ? '' : '/') . 'css/' . $uri;
        }
        if ('.css' !== substr($uri, -4)) {
            $uri .= '.css';
        }

        $this->css[] = [
            'uri'   => $uri,
            'media' => $media,
        ];
    }

    /**
     * Adds a new JS file to the current list, in order to display it when calling $this->render().
     *
     * The name of the file is either an absolute path (when starting with https:// or other protocols) or otherwise a path relative to FCPATH/js/.
     *
     * All the JS will be included in the template, in place of the {template_js} tag.
     *
     * @param string $uri                  The relative or absolute path. The ".js" can optionally be omitted.
     * @param bool   $defer                if the script must be loaded when the DOM is ready (default) or right away
     * @param array  $additionalParameters An associative array containing other pairs of HTML attributes and values. This is intended to be used mostly with `['integrity' => 'sha256-yadaYadaYadaSha256IntegrityHash=', 'crossorigin' => 'anonymous']`
     */
    protected function js(string $uri, bool $defer = true, array $additionalParameters = []): void
    {
        if (! preg_match('#^(\w+:)?//#', $uri)) {
            $app = new App();
            $uri = $app->baseURL . ('/' === substr($app->baseURL, -1) ? '' : '/') . 'js/' . $uri;
        }
        if ('.js' !== substr($uri, -3)) {
            $uri .= '.js';
        }

        $this->js[] = [
            'uri'                  => $uri,
            'defer'                => $defer,
            'additionalParameters' => $additionalParameters,
        ];
    }

    /**
     * Adds a new line of JS that will be displayed as is in the template file.
     *
     * The correct usage is for example: $this->addJavascript("const baseURL = $baseURL;");
     *
     * All the lines will be inserted in the template, surrounded with a script tag, in place of the {template_javascript} tag.
     *
     * @param string $code the line of JS code to append in the file
     */
    protected function addJavascript(string $code): void
    {
        $this->javascript[] = $code;
    }

    /**
     * Acts as a contructor.
     *
     * @return void
     *
     * @codeCoverageIgnore
     */
    protected function init()
    {
    }

    /**
     * Replaces multpile calls to `$this->data['foo'] = $elem['foo']` by inserting all the data into $this->data.
     *
     * Example usage could be:
     *
     * $this->addAllData($user);
     *
     * instead of
     *
     * $this->data['first_name'] = $user['first_name'];
     * $this->data['last_name'] = $user['last_name'];
     * $this->data['email'] = $user['email'];
     * $this->data['birth_date'] = $user['birth_date'];
     * ...
     *
     * @param array $data an associative array containing all the data to add
     */
    protected function addAllData(array $data, ?string $prefix = null): void
    {
        if (null !== $prefix) {
            $prefixedData = [];

            foreach ($data as $key => $value) {
                $prefixedData[$prefix . '_' . $key] = $value;
            }
            $data = $prefixedData;
        }

        $this->data = array_merge($this->data, $data);
    }
}
