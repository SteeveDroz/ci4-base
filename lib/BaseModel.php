<?php

/**
 * This file is created by Steeve Droz and is part of the Ci4Base package.
 */

namespace SteeveDroz\CiBase;

use CodeIgniter\Model;

/**
 * Allows to use helpful methods in a model.
 */
abstract class BaseModel extends Model
{
    /**
     * Applies a callback to each element found by a afterFind event.
     *
     * When registering an event with `protected $afterFind = ['fooBar']`, the corresponding method can call applyOnFind to apply the callback:
     * - to a single entry (when called by first() of find())
     * - to each of the multiple entries (when called by findAll()).
     *
     * The callback must take exactly one argument representing the data of one entry and return it.
     *
     * @param array    $data     the $data provided by the event, as is
     * @param callable $callback the callback to apply to each entry
     *
     * @return ?array the $data, modified, ready to be returned by the event, or null $data was null
     */
    protected function applyOnFind(?array $data, callable $callback): ?array
    {
        if (null === $data) {
            return $data;
        }
        if (array_key_exists('limit', $data)) {
            for ($i = 0; $i < count($data['data']); $i++) {
                $data['data'][$i] = $this->applyOnFind(['data' => $data['data'][$i]], $callback)['data'];
            }
        } elseif (array_key_exists('data', $data) && null !== $data['data']) {
            $data['data'] = $callback($data['data']);
        }

        return $data;
    }
}
