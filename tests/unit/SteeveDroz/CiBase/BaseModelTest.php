<?php

namespace SteeveDroz\CiBase;

use Mocks\PublicBaseModel;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;

/**
 * @internal
 */
#[CoversClass(BaseModel::class)]
final class BaseModelTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();
    }

    #[Test]
    public function constructor()
    {
        $model = new class () extends PublicBaseModel {};

        $this->assertInstanceOf(BaseModel::class, $model);
    }

    #[Test]
    public function applyOnFind()
    {
        $model = new class () extends PublicBaseModel {};

        $data = null;
        $data = $model->applyOnFind($data, static fn ($data) => $data);

        $this->assertNull($data);

        $data = [];
        $data = $model->applyOnFind($data, static fn ($data) => $data);

        $this->assertIsArray($data);
        $this->assertCount(0, $data);

        $data = [
            'data' => [
                'id'      => 1,
                'title'   => 'TITLE',
                'content' => 'CONTENT',
            ],
        ];
        $data = $model->applyOnFind($data, static function ($article) {
            $article['title_content'] = $article['title'] . ' ' . $article['content'];

            return $article;
        });

        $this->assertIsArray($data);
        $this->assertCount(1, $data);
        $this->assertArrayHasKey('data', $data);
        $this->assertIsArray($data['data']);
        $this->assertCount(4, $data['data']);
        $this->assertArrayHasKey('id', $data['data']);
        $this->assertSame(1, $data['data']['id']);
        $this->assertArrayHasKey('title', $data['data']);
        $this->assertSame('TITLE', $data['data']['title']);
        $this->assertArrayHasKey('content', $data['data']);
        $this->assertSame('CONTENT', $data['data']['content']);
        $this->assertArrayHasKey('title_content', $data['data']);
        $this->assertSame('TITLE CONTENT', $data['data']['title_content']);

        $data = [
            'limit' => 2,
            'data'  => [
                [
                    'id'      => 1,
                    'title'   => 'TITLE 1',
                    'content' => 'CONTENT 1',
                ],
                [
                    'id'      => 2,
                    'title'   => 'TITLE 2',
                    'content' => 'CONTENT 2',
                ],
            ],
        ];
        $data = $model->applyOnFind($data, static function ($article) {
            $article['title_content'] = $article['title'] . ' ' . $article['content'];

            return $article;
        });

        $this->assertIsArray($data);
        $this->assertCount(2, $data);
        $this->assertArrayHasKey('limit', $data);
        $this->assertSame(2, $data['limit']);
        $this->assertArrayHasKey('data', $data);
        $this->assertIsArray($data['data']);
        $this->assertCount(2, $data['data']);
        $this->assertIsArray($data['data'][0]);
        $this->assertCount(4, $data['data'][0]);
        $this->assertArrayHasKey('id', $data['data'][0]);
        $this->assertSame(1, $data['data'][0]['id']);
        $this->assertArrayHasKey('title', $data['data'][0]);
        $this->assertSame('TITLE 1', $data['data'][0]['title']);
        $this->assertArrayHasKey('content', $data['data'][0]);
        $this->assertSame('CONTENT 1', $data['data'][0]['content']);
        $this->assertArrayHasKey('title_content', $data['data'][0]);
        $this->assertSame('TITLE 1 CONTENT 1', $data['data'][0]['title_content']);
        $this->assertIsArray($data['data'][1]);
        $this->assertCount(4, $data['data'][1]);
        $this->assertArrayHasKey('id', $data['data'][1]);
        $this->assertSame(2, $data['data'][1]['id']);
        $this->assertArrayHasKey('title', $data['data'][1]);
        $this->assertSame('TITLE 2', $data['data'][1]['title']);
        $this->assertArrayHasKey('content', $data['data'][1]);
        $this->assertSame('CONTENT 2', $data['data'][1]['content']);
        $this->assertArrayHasKey('title_content', $data['data'][1]);
        $this->assertSame('TITLE 2 CONTENT 2', $data['data'][1]['title_content']);
    }
}
