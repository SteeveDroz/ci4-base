<?php

namespace SteeveDroz\CiBase;

use Mocks\PublicBaseController;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;

/**
 * @internal
 */
#[CoversClass(BaseController::class)]
final class BaseControllerTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();
    }

    #[Test]
    public function constructor()
    {
        $baseController = new class () extends PublicBaseController {
            public function getTemplate()
            {
                return $this->template;
            }
        };

        $this->assertInstanceOf(BaseController::class, $baseController);
        $this->assertSame('template', $baseController->getTemplate());
    }

    #[Test]
    public function setTemplate()
    {
        $baseController = new class () extends PublicBaseController {
            public function getTemplate()
            {
                return $this->template;
            }
        };

        $baseController->setTemplate('base-page');
        $this->assertSame('base-page', $baseController->getTemplate());

        $baseController->setTemplate('base-page.html');
        $this->assertSame('base-page', $baseController->getTemplate());
    }

    #[Test]
    public function fragment()
    {
        $baseController = new class () extends PublicBaseController {
            public function setData()
            {
                $this->data['name'] = 'fragment';
            }
        };

        $baseController->setData();

        $output = $baseController->fragment('test-fragment');

        $this->assertSame('This is a test for fragment.', $output);
    }

    #[Test]
    public function fragmentWithExtension()
    {
        $baseController = new class () extends PublicBaseController {
            public function setData()
            {
                $this->data['name'] = 'fragment';
            }
        };

        $baseController->setData();

        $output = $baseController->fragment('test-fragment.html');

        $this->assertSame('This is a test for fragment.', $output);
    }

    #[Test]
    public function fragmentNotFound()
    {
        $baseController = new class () extends PublicBaseController {
            public function fragment(string $page): string
            {
                return parent::fragment($page);
            }
        };

        $this->expectException(\CodeIgniter\View\Exceptions\ViewException::class);
        $baseController->fragment('test-fragment-not-present');
    }

    #[Test]
    public function fragmentScalarBeforeArray()
    {
        $baseController = new class () extends PublicBaseController {
            public function setData()
            {
                $this->data['title']             = 'Title';
                $this->data['title_description'] = 'Description';
                $this->data['elements']          = [
                    [
                        'title' => 'Element 1',
                    ],
                    [
                        'title' => 'Element 2',
                    ],
                ];
            }
        };

        $baseController->setData();

        $output = $baseController->fragment('test-fragment-loop.html');

        $this->assertSame(file_get_contents('./tests/expected/test-fragment-loop.html'), $output);
    }

    #[Test]
    public function fragmentArrayBeforeScalar()
    {
        $baseController = new class () extends PublicBaseController {
            public function setData()
            {
                $this->data['elements'] = [
                    [
                        'title' => 'Element 1',
                    ],
                    [
                        'title' => 'Element 2',
                    ],
                ];
                $this->data['title_description'] = 'Description';
                $this->data['title']             = 'Title';
            }
        };

        $baseController->setData();

        $output = $baseController->fragment('test-fragment-loop.html');

        $this->assertSame(file_get_contents('./tests/expected/test-fragment-loop.html'), $output);
    }

    #[Test]
    public function render()
    {
        $baseController = new class () extends PublicBaseController {
            public function init(): void
            {
                $this->response = \Config\Services::response();
            }
        };
        $baseController->init();

        $baseController->setTemplate('test-render');

        ob_start();
        $baseController->render('test-render-page');
        $output = ob_get_clean();

        $this->assertSame(file_get_contents('./tests/expected/test-render.html'), $output);
    }

    #[Test]
    public function renderFull()
    {
        $baseController = new class () extends PublicBaseController {
            public function init(): void
            {
                $this->response           = \Config\Services::response();
                $this->data['page_title'] = 'title';
            }
        };
        $baseController->init();

        $baseController->setTemplate('test-full');

        $baseController->css('style1', 'screen');
        $baseController->css('style2');

        $baseController->js('script1');
        $baseController->js('script2', false);

        $baseController->addJavascript('const a = 1;');
        $baseController->addJavascript("const b = 'something';");

        ob_start();
        $baseController->render('test-full-page');
        $output = ob_get_clean();

        $this->assertSame(file_get_contents('./tests/expected/test-full.html'), $output);
    }

    #[Test]
    public function renderAutomatic()
    {
        $baseController = new class () extends PublicBaseController {
            public function init(): void
            {
                $this->response = \Config\Services::response();
            }
        };
        $baseController->init();

        $baseController->setTemplate('test-automatic');

        ob_start();
        $baseController->render();
        $output = ob_get_clean();

        $this->assertSame(file_get_contents('./tests/expected/test-automatic.html'), $output);
    }

    #[Test]
    public function renderAutomaticWithGet()
    {
        $this->getRenderAutomaticWithGet();
    }

    public function getRenderAutomaticWithGet()
    {
        $baseController = new class () extends PublicBaseController {
            public function init(): void
            {
                $this->response = \Config\Services::response();
            }
        };
        $baseController->init();

        $baseController->setTemplate('test-automatic');

        ob_start();
        $baseController->render();
        $output = ob_get_clean();

        $this->assertSame(file_get_contents('./tests/expected/test-automatic-with-get.html'), $output);
    }

    #[Test]
    public function renderAutomaticWithPost()
    {
        $this->postRenderAutomaticWithPost();
    }

    public function postRenderAutomaticWithPost()
    {
        $baseController = new class () extends PublicBaseController {
            public function init(): void
            {
                $this->response = \Config\Services::response();
            }
        };
        $baseController->init();

        $baseController->setTemplate('test-automatic');

        ob_start();
        $baseController->render();
        $output = ob_get_clean();

        $this->assertSame(file_get_contents('./tests/expected/test-automatic-with-post.html'), $output);
    }

    #[Test]
    public function renderJs()
    {
        $baseController = new class () extends PublicBaseController {
            public function init(): void
            {
                $this->response = \Config\Services::response();
            }
        };
        $baseController->init();

        $baseController->setTemplate('test-empty');

        $baseController->js('script-a');
        $baseController->js('script-b.js');
        $baseController->js('https://example.com/script-c');
        $baseController->js('https://example.com/script-d.js');
        $baseController->js('script-e', false);
        $baseController->js('script-f.js', false);
        $baseController->js('https://example.com/script-g', false);
        $baseController->js('https://example.com/script-h.js', false);
        $baseController->js('script-i', true);
        $baseController->js('script-j.js', true);
        $baseController->js('https://example.com/script-k', true);
        $baseController->js('https://example.com/script-l.js', true);

        ob_start();
        $baseController->render('test-js');
        $output = ob_get_clean();

        $this->assertSame(file_get_contents('./tests/expected/test-js.html'), $output);
    }

    #[Test]
    public function renderJsAdditionalParameters()
    {
        $baseController = new class () extends PublicBaseController {
            public function init(): void
            {
                $this->response = \Config\Services::response();
            }
        };
        $baseController->init();

        $baseController->setTemplate('test-empty');

        $baseController->js('script-a', false);
        $baseController->js('script-b', false, []);
        $baseController->js('script-c', false, ['class' => 'class-c']);
        $baseController->js('script-d', false, ['integrity' => 'i-n-t-e-g-r-i-t-y', 'crossorigin' => 'anonymous']);

        ob_start();
        $baseController->render('test-js');
        $output = ob_get_clean();

        $this->assertSame(file_get_contents('./tests/expected/test-js-additional-parameters.html'), $output);
    }

    #[Test]
    public function addAllData()
    {
        $baseController = new class () extends PublicBaseController {
            public function init(): void
            {
                $this->addAllData([
                    'a' => 'b',
                    'c' => 'd',
                ]);
            }

            public function getData()
            {
                return $this->data;
            }
        };

        $baseController->init();

        $data = $baseController->getData();

        $this->assertIsArray($data);
        $this->assertCount(2, $data);
        $this->assertArrayHasKey('a', $data);
        $this->assertSame('b', $data['a']);
        $this->assertArrayHasKey('c', $data);
        $this->assertSame('d', $data['c']);
    }

    #[Test]
    public function addAllDataWithPrefix()
    {
        $baseController = new class () extends PublicBaseController {
            public function init(): void
            {
                $this->addAllData([
                    'a' => 'b',
                    'c' => 'd',
                ], 'something');
            }

            public function getData()
            {
                return $this->data;
            }
        };

        $baseController->init();

        $data = $baseController->getData();

        $this->assertIsArray($data);
        $this->assertCount(2, $data);
        $this->assertArrayHasKey('something_a', $data);
        $this->assertSame('b', $data['something_a']);
        $this->assertArrayHasKey('something_c', $data);
        $this->assertSame('d', $data['something_c']);
    }
}
