<?php

namespace Mocks;

use SteeveDroz\CiBase\BaseModel;

class PublicBaseModel extends BaseModel
{
    public function applyOnFind(?array $data, callable $callback): ?array
    {
        return parent::applyOnFind($data, $callback);
    }
}
