<?php

namespace Mocks;

use SteeveDroz\CiBase\BaseController;

class PublicBaseController extends BaseController
{
    protected $backtraceLength = 2;

    public function setTemplate(string $template): void
    {
        parent::setTemplate($template);
    }

    public function fragment(string $page): string
    {
        return parent::fragment($page);
    }

    public function render(?string $page = null): void
    {
        parent::render($page);
    }

    public function css(string $uri, string $media = 'all'): void
    {
        parent::css($uri, $media);
    }

    public function js(string $uri, bool $defer = true, array $additionalParameters = []): void
    {
        parent::js($uri, $defer, $additionalParameters);
    }

    public function addJavascript(string $code): void
    {
        parent::addJavascript($code);
    }
}
